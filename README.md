# README

**Contents**

[Overview](#overview)

[Repository](#repository)

[Building a local version](#building-a-local-version)

[Updating this document](#updating-this-document)

[Revision history and versioned PDFs](#revision-history-and-versioned-pdfs)

[Sphinx syntax](#sphinx-syntax)

## Overview

This document exists as a Sphinx project inside a Git repository. For more information on how we use Sphinx/Git/GitLab as a technical documentation toolset, see http://egamitech.gitlab.com/procedures/documentation-guidelines/.

See the most recent draft output of this document directly at:

http://demo.docs-as-code.com

## Repository

Navigate to the address below to access the GitLab project page, or use it directly to clone the repository.

https://gitlab.com/egamid/wi5014-laser-etching-machine.git

### Sphinx structure

See https://egamitech.gitlab.com/procedures/documentation-guidelines/creating-a-sphinx-project/#sphinx-structure

### Key files

See https://egamitech.gitlab.com/procedures/documentation-guidelines/creating-a-sphinx-project/#key-files

## Building a local version

Install the [Toolset](https://egamitech.gitlab.com/procedures/documentation-guidelines/toolset), you can then build a local HTML version and test any updates you make. Do this before updating the repository to ensure that the build works.

```bash
git clone https://gitlab.com/egamid/wi5014-laser-etching-machine.git
cd wi5014-laser-etching-machine
git fetch origin develop
git checkout develop
make html
sphinx-serve
```

Access in your web browser at 127.0.0.1:8080.

See [Creating a Sphinx project: Demo](https://egamitech.gitlab.com/procedures/documentation-guideline/creating-a-sphinx-project/#demo) for more information.

## Updating this document

See [Contributing to documentation](https://egamitech.gitlab.com/procedures/documentation-guidelines/contributing-to-documentation/) for full details. Key methods to update this document:

* Create an issue in the GitLab repository and assign a technical writer
* Create your own feature branch and merge it into *develop*. Ensure you can build the document locally as HTML before merging.

## Revision history and versioned PDFs

* See https://gitlab.com/egamid/wi5014-laser-etching-machine/-/tags for any releases, including details on downloading versioned PDFs.
* Download the most recently released PDF (if applicable) via the *Open PDF* button on the side bar in the draft environment, or see the GitLab's repository's description. 
* For comparing versions, use git's *diff* or *pretty-diff* functionality. See the [Analyzing document changes](https://egamitech.gitlab.com/procedures/documentation-guidelines/analyzing-document-changes/) for useful commands.

## Sphinx syntax

We use *ReStructured* text as our wysiwyg language. For full cheat sheet, see https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html. See below for key syntax.

For key syntax, see https://egamitech.gitlab.com/procedures/documentation-guidelines/creating-a-sphinx-project/#sphinx-syntax.