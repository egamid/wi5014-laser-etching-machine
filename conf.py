import subprocess # allows us to pull in the latext git commit for the displayed content using the commit_id_develop tag below, from https://stackoverflow.com/questions/61579937/how-to-access-the-git-commit-id-in-sphinxs-conf-py. List most recent tag via `git describe --tags --abbrev=0` using commit_id_develop as example
import sphinx_rtd_theme

project = u'WI5014-Laser-Etching-Machine' # Unique name for the document, mirrors the gitalb_repo name. Update in *_static/latex/mystle.sty* also.
family = u'' # Product family
product = u'Laser Etching Machine' # Product name
version = u'v1.4' # Appears on left hand side menu. Update below in the maketitle preamble and in source/_static/latex/mystle.sty if needed. Can also replace with datetime.date.today().strftime('%Y-%m-%d') and used in conjunction with the datetime2 package in source/_static/latex/mystle.sty
master_doc = 'index' # Leave as is, this needs to be above *latex_documents*
copyright = u'Your Company' # if different to author, otherwise put author
author = u'Your Company'
commit_id = commit_id = subprocess.check_output(['git', 'rev-parse', '--short=8', 'HEAD']).strip().decode('ascii')
commit_date = subprocess.check_output(['git', 'log', '-1', '--date=format:%Y-%m-%d %H:%M UTC', '--format=%ad', 'HEAD']).strip().decode('ascii')
#most_recent_tag = subprocess.check_output(['git', 'describe', '--tags', '--abbrev=0']).strip().decode('ascii')
#most_recent_tag = subprocess.check_output(['git', 'describe', '--tags', '--abbrev=0']).strip().decode('ascii')

# these values appear in the document
rst_prolog = """
.. |product| replace:: {product_prolog}
.. |company| replace:: {company_prolog}
.. |legal| replace:: {legal_prolog}
.. |commit_id| replace:: {commit_id_prolog}
.. |commit_date| replace:: {commit_date_prolog}
""".format(family_prolog = family,product_prolog = product,company_prolog = author,legal_prolog = copyright,commit_id_prolog = commit_id,commit_date_prolog = commit_date) # this format function allows us to pass variables into the prolog. Use it in the same way for the epilog

#.. |button-focus| image:: figures/button-focus.png
#    :width: 10px
#.. |button-return| image:: figures/button-return.png
#    :width: 10px

html_context = { # these settings are for the html template at source/_templates/layout.html
    "family": family,
    "product": product,
    "gitlab_group": "egamid", # gitlab reference to the group
    "gitlab_repo": project, # gitlab reference to project, this should mirror the *project* variable in lowercase
    "commit_id": commit_id,
    "commit_date": commit_date,
    #"most_recent_tag": most_recent_tag,
}

latex_engine = 'xelatex'
latex_elements = {
    'papersize': 'a4paper', # a4paper/letterpaper
    'pointsize': '11pt', # Change linespacing via source/_static/latex/mystle.sty or for the title spage via maketitle preamble below
    'figure_align': 'H',
    'sphinxsetup':'hmargin={2cm}, vmargin={2.5cm}, verbatimwithframe=true, TitleColor={rgb}{0,0,0}, OuterLinkColor={rgb}{0.208,0.374,0.486}, shadowsize=2pt, dangerborder=0.5pt, warningborder=0.5pt, cautionborder=0.5pt, attentionborder=0.5pt,',
    'printindex':"",
    'preamble': r'\usepackage{mystyle}', # Adjust latex preamble in this file, ensure you set the correct document properties within
    # Use maketitle below to change title page and header/footer. This inserts content AFTER the document start tag in latex
    'maketitle': r'''
    
        %%% Titlepage %%%

        \begin{titlepage}
        \newgeometry{left=6cm}
        \newcommand{\colorRule}[3][black]{\textcolor[HTML]{ 343a40 }{\rule{#2}{#3}}} % Adjust line colour here
        \begin{flushleft}
        \noindent
        \\[-1em]
        \color[HTML]{5F5F5F}
        \makebox[0pt][l]{\colorRule[ 343a40 ]{1.3\textwidth}{4pt}}  % Adjust line colour here
        \par
        \noindent
        
        {
        \vfill
        \noindent \begin{spacing}{2}{\huge \textbf{\textsf{WI5014 Laser Etching Machine}}} % Add project here
        \vskip 1em
        {\Large \textsf{Electrical Specifications}} % Add description here
        \vskip 1em
        \noindent
        {\Large \textsf{\version}} % Add document version via datetime2 or version number here
        \end{spacing}
        \vfill
        }
        
        \noindent
        \includegraphics[width=100pt]{../../_static/images/logo-main.png}
        \vskip 1em
        \textsf{\textcopyright2020 Your company} % Add copyright information here
        
        \end{flushleft}
        \end{titlepage}
        \restoregeometry
        
        %%% End titlepage %%%
        
        %%% Header and footer %%%
        
        \fancypagestyle{plain}{
         \fancyhf{}
         \fancyhead[L]{\scriptsize \textsf{Laser Etching Machine}}
         \fancyhead[R]{\scriptsize \textsf{\href{https://demo.docs-as-code.com}{Online version}}} % address to document homepage on CMS
         \fancyfoot[R]{\scriptsize \textsf{Your company}}
         \fancyfoot[L]{\scriptsize \textsf{WI5014-\version}}
         \fancyfoot[C]{\scriptsize \textsf{Page \thepage}}
         \renewcommand{\headrulewidth}{0.4pt}
         \renewcommand{\footrulewidth}{0.4pt}
        }
        
        \fancypagestyle{normal}{
         \fancyhf{}
         \fancyhead[L]{\scriptsize \textsf{Laser Etching Machine}}
         \fancyhead[R]{\scriptsize \textsf{\href{https://demo.docs-as-code.com}{Online version}}} % address to document homepage on CMS
         \fancyfoot[R]{\scriptsize \textsf{Your company}}
         \fancyfoot[L]{\scriptsize \textsf{WI5014-\version}}
         \fancyfoot[C]{\scriptsize \textsf{Page \thepage}}
         \renewcommand{\headrulewidth}{0.4pt}
         \renewcommand{\footrulewidth}{0.4pt}
        }
        
        %%% End header and footer %%%
    ''',
}

# Manually update *project*-*version* and *project* according to above variables
latex_documents = [
    (master_doc, project+'.tex', project, author, 'howto'),
]

# -- General configuration ---------------------------------------------------
release = version
numfig = True
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.autosectionlabel',
    'sphinx_rtd_theme',
    ]

# The below epilog adds a line break function for the tables. Use via |br| in your table
rst_epilog = """
.. role:: raw-html(raw)
   :format: html
   
.. role:: raw-latex(raw)
   :format: latex
   
.. |br| replace:: :raw-html:`<br>`:raw-latex:`\linebreak`
"""

autosectionlabel_prefix_document = True
templates_path = ['_templates']
source_suffix = {
    '.rst': 'restructuredtext',
}    
language = None
exclude_patterns = ['_build', '_drafts']
pygments_style = None
html_theme = "sphinx_rtd_theme"

def setup(app):
    app.add_css_file('custom.css') # custom overrides for RTD theme

html_show_sourcelink = False
html_static_path = ['_static']
latex_additional_files = ['_static/mystyle.sty']
latex_show_pagerefs = True
latex_show_urls = 'inline'
intersphinx_mapping = {'https://docs.python.org/': None}
todo_include_todos = True
todo_emit_warnings = True
latex_use_xindy = False # Xindy breaks the gitlab CI, could also try adding `apt install xindy` to gitlab-ci.yml