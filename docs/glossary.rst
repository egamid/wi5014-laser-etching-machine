********
Glossary
********

.. Add terms using the same structure as the existing terms. This is a cascading hierarchy of the term, the expanded term (if applicable), and the definition. Each item is indented by one space.

.. glossary::
    :sorted:
    
    Lorem
     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    
    Ipsum
     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
     
    Dolor
     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.