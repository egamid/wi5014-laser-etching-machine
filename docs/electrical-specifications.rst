*************************
Electrical specifications
*************************

.. versionchanged:: 1.3

    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

.. note:: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

Absolute maximum ratings
========================
   
.. attention:: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

.. table:: Absolute maximum ratings
  :name: absolute-maximum-ratings
  :class: longtable
  :widths: 15,13,48,8,8,8
  :align: left

  +-----------------+----------------------+--------------------------------------------------------------------+-------+-------+------+
  | Aspect          | Parameter            | Description/condition                                              | MIN   | MAX   | Unit |
  +=================+======================+====================================================================+=======+=======+======+
  | Electrical      | V\ :sub:`DDS`        | Lorem ipsum dolor sit amet                                         | \-    | 5.433 | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`DDH`        | Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet             | \-    | 2.67  | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`DDA`        | Lorem ipsum dolor sit amet                                         | \-    | 3.4   | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`DDD`        | Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet              | \-    | 6.3   | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`DDH2`       | Lorem ipsum dolor sit amet                                         | \-    | ± 0.1 | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`DDH3`       | Lorem ipsum dolor sit ametLorem ipsum dolor sit amet               | –0.89 | 3.4   | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`DDA2`       | Lorem ipsum dolor sit amet                                         | –0.21 | 2.1   | V    |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | V\ :sub:`CMOS`       | Lorem ipsum dolor sit amet                                         | –0.72 | 3.928 | V    |
  +-----------------+----------------------+--------------------------------------------------------------------+-------+-------+------+
  | Electrostatic   | ESD\ :sub:`HBM`      | Lorem ipsum dolor sit amet                                         | \-    | ±510  | V    |
  | discharge (ESD) |                      | \ :sup:`(1)`                                                       |       |       |      |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | ESD\ :sub:`CSM`      | Lorem ipsum dolor sit amet\ :sup:`(2)`                             | \-    | ±515  | V    |
  +-----------------+----------------------+--------------------------------------------------------------------+-------+-------+------+
  | Environmental   | T\ :sub:`FHF`        | Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet              | -85   | 350   | °C   |
  | conditions      |                      |                                                                    |       |       |      |
  |                 +----------------------+--------------------------------------------------------------------+-------+-------+------+
  |                 | LSS                  | Lorem ipsum dolor sit amet                                         | 5     | \-    | \-   |
  +-----------------+----------------------+--------------------------------------------------------------------+-------+-------+------+
   
.. compound::
  :class: table-notes

  **Table notes:**

  \(1) Lorem ipsum dolor sit amet

  \(2) Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
    
.. raw:: latex

  \clearpage

Recommended operating conditions
================================

.. versionchanged:: 1.4

  Updated several values in :numref:`Table {number} <Typical operation parameters>`.
  
.. versionchanged:: 1.2

    Lorem ipsum dolor sit amet
    
.. table:: Recommended operating conditions
  :name: Typical operation parameters
  :class: longtable
  :align: left

  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | Parameter      | Description/condition                                        | MIN   | TYP  | MAX   | Unit |
  +================+==============================================================+=======+======+=======+======+
  | V\ :sub:`DDS`  | Lorem ipsum dolor sit amet\ :sup:`(1)`                       | 3.553 | 2.22 | 3.333 | V    |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`DDH`  | Lorem ipsum dolor sit amet\ :sup:`(1)`                       | 7.33  | 3.22 | 4.58  | V    |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`DDA`  | Lorem ipsum dolor sit amet\ :sup:`(1)`                       | 5.25  | 1.55 | 2.63  | V    |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`DDD`  | Lorem ipsum dolor sit amet\ :sup:`(1)`                       | 2.11  | 3.44 | 6.55  | V    |
  |                +--------------------------------------------------------------+-------+------+-------+------+
  |                | Lorem ipsum dolor sit amet (V\ :sub:`DDD` = V\ :sub:`DDA`)   | 6.33  | 6.2  | 4.22  | V    |
  |                | \ :sup:`(1), (2)`                                            |       |      |       |      |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`KSSS` | Lorem ipsum dolor sit amet V\ :sub:`DDS`                     | \-    | \-   | 22    | mVpp |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`SSSD` | Lorem ipsum dolor sit amet V\ :sub:`DDH`                     | \-    | \-   | 55    | mVpp |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`LMSY` | Lorem ipsum dolor sit amet V\ :sub:`DDA`                     | \-    | \-   | 77    | mVpp |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | V\ :sub:`PSJS` | Lorem ipsum dolor sit amet V\ :sub:`DDD`                     | \-    | \-   | 34    | mVpp |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | T\ :sub:`G`    | Lorem ipsum dolor sit ametLorem ipsum dolor sit amet         | 0     | \-   | 39    | °C   |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+
  | T\ :sub:`P`    | Lorem ipsum dolor sit ametLorem ipsum dolor sit amet         | 0     | \-   | 110   | °C   |
  +----------------+--------------------------------------------------------------+-------+------+-------+------+

.. compound::
  :class: table-notes

  **Table notes:**

  \(1) Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.

  \(2) Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
   

Thermal information
===================

.. versionchanged:: 1.3

   Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.

.. note:: Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.

.. table:: Thermal information specifications
  :class: longtable
  :align: left
  :name: thermal-information

  +---------------+----------------------------------------+-------+------+
  | Parameter     | Thermal characteristics                | Value | Unit |
  +===============+========================================+=======+======+
  | R\ :sub:`θGA` | Lorem ipsum dolor sit amet\ :sup:`(1)` | 12.45 | °C/W |
  +---------------+----------------------------------------+-------+------+
  | R\ :sub:`θHH` | Lorem ipsum dolor sit amet             | 8.000 | °C/W |
  +---------------+----------------------------------------+-------+------+
  | R\ :sub:`θLS` | Lorem ipsum dolor sit amet             | 15.2  | °C/W |
  +---------------+----------------------------------------+-------+------+
  | Ψ\ :sub:`PP`  | Lorem ipsum dolor sit amet             | 15.2  | °C/W |
  +---------------+----------------------------------------+-------+------+

.. compound::
  :class: table-notes

  **Table notes:**
      
  \(1) Lorem ipsum dolor sit amet
