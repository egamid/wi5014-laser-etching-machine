.. only:: latex

    .. raw:: latex
       
       \let\cleardoublepage\clearpage
       \listoftables
       \listoffigures
       \let\clearpage\cleardoublepage
       \clearpage

.. raw:: latex

    \clearpage
    
*********************
Laser Etching Machine
*********************

Document scope
  .. warning:: This document is an example. Content within it is made up.

  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      
Revision history
  We electronically control revisions for this document. Any copies you generate or print from the latest revision are uncontrolled.

  This content was generated via commit |commit_id| at |commit_date|.

  .. only:: latex

     For a full set of feedback tools and to ensure you are viewing the most recent revision, access this document online via the link in the header.
   
  .. list-table:: Changes between v1.3 and v1.4
     :widths: 25,75
     :align: left
     :class: longtable
     :header-rows: 1

     * - **Section**
       - **Description**
     * - Electrical specifications
       - Updated several values in :numref:`Table {number} <Typical operation parameters>`.

  .. list-table:: Changes between v1.2 and v1.3
      :widths: 25,75
      :align: left
      :class: longtable
      :header-rows: 1

      * - **Section**
        - **Description**
      * - Device description
        - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. :numref:`Fig. {number} <placeholder>`.
      * - Electrical specifications
        - Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      * - Electrical specifications
        - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    
.. raw:: latex

    \clearpage
    
.. toctree::
    :maxdepth: 3
    :numbered:

    docs/device-description
    docs/electrical-specifications
    docs/glossary
